#!/bin/bash

export SW_DIR=$(dirname $0)  # Could this be directly in the original config.sh?  
cd $SW_DIR     

ROOT_DIR="$HOME/Workspace/Python/secstrannotator-dev-data-tmp"
INIT_DIR="$SW_DIR/init"
export VAR_DIR="$ROOT_DIR/var"
export DATA_DIR="$ROOT_DIR/data"
export SECSTRANNOTATOR_DLL="$HOME/Workspace/C#/SecStrAnnot2/bin/Debug/net6.0/SecStrAnnotator.dll"
export CIF_SOURCES="http://www.ebi.ac.uk/pdbe/entry-files/download/{pdb}_updated.cif.gz  http://www.ebi.ac.uk/pdbe/entry-files/download/{pdb}_updated.cif"
export RQ_QUEUE='sestra_jobs'
export N_RQ_WORKERS=4
export N_GUNICORN_WORKERS=4
export GUNICORN_PORT='4000'

# Preparation
source venv/bin/activate

# REDIS-QUEUE
rq worker $RQ_QUEUE  & # run repeatedly to start more workers

# FLASK
export FLASK_APP='secstrannotator_server'
export FLASK_ENV='development'
flask run
