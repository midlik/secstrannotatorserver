import flask
import markupsafe
import uuid
import json
from datetime import timedelta
from http import HTTPStatus
from pathlib import Path
from contextlib import suppress
from typing import Any, NamedTuple, Optional, Union

from .constants import DATA_DIR, DB_DIR_PENDING, DB_DIR_RUNNING, DB_DIR_COMPLETED, DB_DIR_ARCHIVED, DB_DIR_FAILED, JOB_RESULTS_DIR, JOB_ERROR_MESSAGE_FILE, REFRESH_TIMES, DEFAULT_FAMILY_EXAMPLE, LAST_UPDATE_FILE, JOB_PARAMS
from . import queuing
from . import job_processing


app = flask.Flask(__name__)


# CONTROLLERS 

@app.route('/')
def index() -> Any:
    '''Redirect to the main page (Home)'''
    return flask.redirect('/home')

@app.route('/home')
def home() -> Any:
    '''Show home page'''
    return flask.render_template('home.html')

@app.route('/manual')
def manual() -> Any:
    '''Show manual page'''
    return flask.redirect('https://webchem.ncbr.muni.cz/Wiki/SecStrAnnotator')
    # TODO create SecStrAnnotator Online manual and reference SecStrAnnotator manual

@app.route('/submit')
def submit() -> Any:
    '''Show submission form'''
    values = flask.request.values  # flask.request.args from GET + flask.request.form from POST
    job_name = values.get('job_name', type=str, default='')
    domain_list = values.get('domain_list', type=str, default='').replace(';', '\n')
    return flask.render_template('submit.html', job_name=job_name, domain_list=domain_list)

@app.route('/submission', methods=['POST'])
def submission_post() -> Any:
    '''Submit a job. Then redirect to Submitted page or Invalid submission page.'''
    values = flask.request.values  # flask.request.args from GET + flask.request.form from POST
    job_name = values.get('job_name', type=str).strip()
    template_type = values.get('template_type', type=str)
    query_type = values.get('query_type', type=str)
    query_chain = values.get('query_chain', type=str, default='').strip() or '.'
    query_range = values.get('query_range', type=str, default='').replace(' ', '') or ':'
    all_chains = bool(values.get('all_chains'))
    if all_chains:
        if template_type != 'none':
            return ResponseTuple.plain(f'"all_chains" can be true only if "template_type" is "none"', status=HTTPStatus.BAD_REQUEST)
        query_chain = None

    job_params = job_processing.JobParams(job_name=job_name, template_type=template_type, template_name='', query_type=query_type, query_name='', query_chain=query_chain, query_range=query_range)
    job_files = {}
    if template_type == 'standard':
        template_standard = values.get('template_standard', type=str, default='')
        job_params.template_standard = template_standard
        job_params.template_name = template_standard
    elif template_type == 'custom':
        template_struct_file = flask.request.files['template_structure_file']
        if template_struct_file.filename == '':
            message = f'No template structure file selected. Please select template structure file.'
            return flask.redirect(flask.url_for('invalid_submission', message=message))
        job_params.template_name = Path(template_struct_file.filename).stem
        file_ext = Path(template_struct_file.filename).suffix.lower()
        if file_ext == '.cif':
            job_files['template.cif'] = template_struct_file
        elif file_ext == '.pdb' or file_ext == '.ent':
            job_files['template.pdb'] = template_struct_file
        else:
            message = f'Template structure file must be in mmCIF or PDB format. Please select a file with extension .cif or .pdb'
            return flask.redirect(flask.url_for('invalid_submission', message=message))
        template_annot_file = flask.request.files['template_annotation_file']
        if template_annot_file.filename == '':
            message = f'No template annotation file selected. Please select template annotation file.'
            return flask.redirect(flask.url_for('invalid_submission', message=message))
        job_files['template-template.sses.json'] = template_annot_file
    elif template_type == 'none':
        job_params.template_name = '-'
    else:
        return ResponseTuple.plain(f'"template_type" must be "standard"|"custom"|"none", not {template_type}', status=HTTPStatus.BAD_REQUEST)

    if query_type == 'pdb':
        query_pdbid = values.get('query_pdbid', type=str, default='').strip().lower()
        job_params.query_pdbid = query_pdbid
        job_params.query_name = query_pdbid
    elif query_type == 'upload':
        query_file = flask.request.files['query_structure_file']
        if query_file.filename == '':
            message = f'No query structure file selected. Please select query structure file.'
            return flask.redirect(flask.url_for('invalid_submission', message=message))
        job_params.query_name = Path(query_file.filename).stem
        file_ext = Path(query_file.filename).suffix.lower()
        if file_ext == '.cif':
            job_files['query.cif'] = query_file
        elif file_ext == '.pdb' or file_ext == '.ent':
            job_files['query.pdb'] = query_file
        else:
            message = f'Query structure file must be in mmCIF or PDB format. Please select a file with extension .cif or .pdb'
            return flask.redirect(flask.url_for('invalid_submission', message=message))
    elif query_type is None:
        message = f'Please select input structure.'
        return flask.redirect(flask.url_for('invalid_submission', message=message))
    else:
        return ResponseTuple.plain(f'"query_type" must be "pdb"|"upload", not {query_type}', status=HTTPStatus.BAD_REQUEST) 

    job_status, qid = queuing.enqueue_job(job_name, job_params, job_files)
    job_id = job_status.id
    return flask.redirect(flask.url_for('submitted', job_id=job_id))

@app.route('/submitted/<string:job_id>')
def submitted(job_id: str) -> Any:
    '''Show Submitted page (meaning that the job has been submitted successfully), which just sends GoogleAnalytics and redirects to /job/<job_id>.'''
    return flask.render_template('submitted.html', job_id=job_id)

@app.route('/invalid_submission')
def invalid_submission() -> Any:
    '''Show Invalid submission page, meaning that data in the submitted form are invalid. (In case that the form validation failed to reject the data).'''
    values = flask.request.values  # flask.request.args from GET + flask.request.form from POST
    message = values.get('message', type=str).strip()
    return flask.render_template('invalid_submission.html', message=message)

@app.route('/job/<string:job_id>')
def job(job_id: str) -> Any:
    '''Show a page with the status of the job (Pending/Completed/Failed...) and results (if Completed/Archived).'''
    job_status = queuing.JobStatusInfo.load_for_job(job_id)
    if job_status.status == queuing.JobStatus.NONEXISTENT:
        message = markupsafe.Markup(f'Job <strong>{markupsafe.escape(job_id)}</strong> does not exist.')
        return flask.render_template('not_found.html', message=message), HTTPStatus.NOT_FOUND
    elif job_status.status in [queuing.JobStatus.PENDING, queuing.JobStatus.RUNNING]:
        now = queuing._get_current_time()
        elapsed_time = now - job_status.submission_time
        refresh_time = _calculate_time_to_refresh(elapsed_time)
        url = flask.request.url
        return flask.render_template('waiting.html', job_id=job_id, job_name=job_status.name, job_status=job_status.status.value, 
                                     submission_time=job_status.submission_time, current_time=now, elapsed_time=elapsed_time, 
                                     url=url, refresh_time=refresh_time)
    elif job_status.status in [queuing.JobStatus.COMPLETED, queuing.JobStatus.ARCHIVED]:
        params_file = _get_job_file(job_id, JOB_PARAMS)
        params = json.loads(params_file.read_text())
        try:
            summary_file = _get_job_file(job_id, JOB_RESULTS_DIR, 'summary.json')
            summary = json.loads(summary_file.read_text())
        except FileNotFoundError:
            summary = {}
        try:
            vis_file = _get_job_file(job_id, JOB_RESULTS_DIR, 'query-vis-html.txt')
            visual = vis_file.read_text().rstrip('\n')
        except FileNotFoundError:
            visual = ''
        pse_file: Optional[Path]
        try:
            pse_file = _get_job_file(job_id, JOB_RESULTS_DIR, 'query-annotated.pse')
        except FileNotFoundError:
            try: 
                pse_file = _get_job_file(job_id, JOB_RESULTS_DIR, 'query-detected.pse')
            except FileNotFoundError:
                pse_file = None
        pse_available = pse_file is not None
        return flask.render_template('completed.html', job_id=job_id, job_name=job_status.name, job_status=job_status.status.value, 
                                    submission_time=job_status.submission_time, template_name=params['template_name'], query_name=params['query_name'],
                                    params=params,
                                    summary=summary,
                                    pse_available=pse_available,
                                    visual=flask.Markup(visual),
                                    )
    elif job_status.status == queuing.JobStatus.FAILED:
        try:
            error_message = Path.read_text(DB_DIR_FAILED/job_id/JOB_ERROR_MESSAGE_FILE)
        except FileNotFoundError:
            error_message = ''
        return flask.render_template('failed.html', job_id=job_id, job_name=job_status.name, job_status=job_status.status.value, 
                                    submission_time=job_status.submission_time, error_message=error_message)
    elif job_status.status == queuing.JobStatus.DELETED:
        message = markupsafe.Markup(f'Job <strong>{markupsafe.escape(job_id)}</strong> expired ({job_status.delete_time}) and has been deleted.')
        return flask.render_template('not_found.html', title='Expired', message=message), HTTPStatus.NOT_FOUND
    else:
        raise AssertionError(f'Unknown job status: {job_status.status}')

@app.route('/favicon.ico')
def favicon() -> Any:
    '''Send favicon (the icon shown in browser tab header).'''
    return flask.send_file('static/images/favicon.ico')

@app.route('/logos/<string:template>')
def logos(template: str) -> Any:
    '''Show page with all sequence logos'''
    return flask.redirect('https://webchem.ncbr.muni.cz/Wiki/SecStrAnnotator:Analysis#Sequence_of_SSEs')

@app.route('/results/<string:job_id>/<path:file>')
def results(job_id: str, file: str) -> Any:
    '''Send a file from the job's results directory'''
    # if '..' in file:
    #     message = markupsafe.Markup(f'File <strong>{markupsafe.escape(file)}</strong> for job <strong>{markupsafe.escape(job_id)}</strong> is not available.')
    #     return flask.render_template('not_found.html', message=message), HTTPStatus.FORBIDDEN
    with suppress(FileNotFoundError):
        return flask.send_file(DB_DIR_COMPLETED/job_id/JOB_RESULTS_DIR/file)
    with suppress(FileNotFoundError):
        return flask.send_file(DB_DIR_ARCHIVED/job_id/JOB_RESULTS_DIR/file)
    with suppress(FileNotFoundError):
        return flask.send_file(DB_DIR_FAILED/job_id/JOB_RESULTS_DIR/file)
    message = markupsafe.Markup(f'File <strong>{markupsafe.escape(file)}</strong> for job <strong>{markupsafe.escape(job_id)}</strong> does not exist.')
    return flask.render_template('not_found.html', message=message), HTTPStatus.NOT_FOUND


@app.route('/data/<path:file>')
def data(file: str):
    '''Send a static file from the server data directory (for development, in production this request will be served by Nginx).'''
    # if '..' in file:
    #     message = markupsafe.Markup(f'File <strong>{markupsafe.escape(file)}</strong> is not available.')
    #     return flask.render_template('not_found.html', message=message), HTTPStatus.FORBIDDEN
    try:
        return flask.send_file(DATA_DIR/file)
    except FileNotFoundError:
        message = markupsafe.Markup(f'File <strong>{markupsafe.escape(file)}</strong> does not exist.')
        return flask.render_template('not_found.html', message=message), HTTPStatus.NOT_FOUND


# HELPER FUNCTIONS AND CLASSES

class ResponseTuple(NamedTuple):
    '''Tuple that can be returned by a controller'''
    response: Union[str, dict]
    status: Optional[int] = None
    headers: Union[list, dict, None] = None
    @classmethod
    def plain(cls, response, status=None, headers=None) -> 'ResponseTuple':
        if headers is None:
            headers = {'Content-Type': 'text/plain; charset=utf-8'}
        else:
            headers['Content-Type'] = 'text/plain; charset=utf-8'
        return cls(str(response), status, headers)

def _get_uuid() -> uuid.UUID:
    return uuid.uuid4()

def _get_last_update() -> str:
    try:
        return Path.read_text(LAST_UPDATE_FILE).strip()
    except OSError:
        return '???'

def _get_job_file(job_id: str, *path_parts: str) -> Path:
    '''Get path to file jobs/*/job_id/*path_parts of given job, where * can be Completed, Archived, Failed, or Pending etc. '''
    for db_dir in (DB_DIR_COMPLETED, DB_DIR_ARCHIVED, DB_DIR_FAILED, DB_DIR_PENDING):
        path = Path(db_dir, job_id, *path_parts)
        if path.exists():
            return path
    raise FileNotFoundError('/'.join(['...', job_id, *path_parts]))

def _calculate_time_to_refresh(elapsed_time: timedelta) -> int:
    elapsed_seconds = int(elapsed_time.total_seconds())
    try:
        next_refresh = next(t for t in REFRESH_TIMES if t > elapsed_seconds)
        return next_refresh - elapsed_seconds
    except StopIteration:
        return -elapsed_seconds % REFRESH_TIMES[-1]
