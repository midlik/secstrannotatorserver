import rq  # type: ignore
import redis
from enum import Enum
from pathlib import Path
from contextlib import suppress
from datetime import datetime, timedelta
import uuid
from dataclasses import dataclass, field
from typing import List, Tuple, Dict, Optional, Any, Union, Literal

from .constants import DB_DIR_PENDING, DB_DIR_RUNNING, DB_DIR_COMPLETED, DB_DIR_FAILED, DB_DIR_ARCHIVED, DB_DIR_DELETED, JOB_STATUSINFO_FILE, JOB_PARAMS, JOB_DATADIR, JOB_RESULT_FILE, TIME_FORMAT, COMPLETED_JOB_STORING_DAYS, QUEUE_NAME, JOB_TIMEOUT, JOB_CLEANUP_TIMEOUT
from . import job_processing
from .lib import move


def enqueue_job(job_name: str, params: job_processing.JobParams, files: dict) -> Tuple['JobStatusInfo', Any]:
    job_status = JobStatusInfo(name=job_name)  # default status Pending
    job_id = job_status.id
    Path.mkdir(DB_DIR_PENDING/job_id, parents=True, exist_ok=True)
    job_status.save(DB_DIR_PENDING/job_id/JOB_STATUSINFO_FILE)
    Path.write_text(DB_DIR_PENDING/job_id/JOB_PARAMS, params.to_json())
    Path.mkdir(DB_DIR_PENDING/job_id/JOB_DATADIR, exist_ok=True)
    for file_name, file in files.items():
        file.save(DB_DIR_PENDING/job_id/JOB_DATADIR/file_name)

    queue = rq.Queue(QUEUE_NAME, connection=redis.Redis.from_url('redis://'), default_timeout=JOB_TIMEOUT+JOB_CLEANUP_TIMEOUT)
    job = queue.enqueue(process_job, job_id)
    queue_id = job.get_id()
    return job_status, queue_id


def process_job(job_id: str):
    job_dir = DB_DIR_RUNNING/job_id
    move(DB_DIR_PENDING/job_id, job_dir)
    job_status = JobStatusInfo.update(job_dir/JOB_STATUSINFO_FILE, status=JobStatus.RUNNING, start_time=_get_current_time())
    assert job_status.id == job_id

    params = job_processing.JobParams.from_json(Path.read_text(job_dir/JOB_PARAMS))
    with open(job_dir/JOB_RESULT_FILE, 'w') as w:
        for k, v in vars(params).items():
            w.write(f'{k}: {v}\n\n')
        w.write('Running...\n\n')

    successfull = job_processing.run_job(params, job_dir)

    end_time = _get_current_time()
    delete_time = end_time + timedelta(COMPLETED_JOB_STORING_DAYS)
    if successfull:
        Path.mkdir(DB_DIR_COMPLETED, parents=True, exist_ok=True)
        move(job_dir, DB_DIR_COMPLETED/job_id)
        job_status = JobStatusInfo.update(DB_DIR_COMPLETED/job_id/JOB_STATUSINFO_FILE, status=JobStatus.COMPLETED, end_time=end_time, delete_time=delete_time)
    else:
        Path.mkdir(DB_DIR_FAILED, parents=True, exist_ok=True)
        move(job_dir, DB_DIR_FAILED/job_id)
        job_status = JobStatusInfo.update(DB_DIR_FAILED/job_id/JOB_STATUSINFO_FILE, status=JobStatus.FAILED, end_time=end_time, delete_time=delete_time)


def _get_uuid() -> str:
    return str(uuid.uuid4())

def _get_current_time() -> datetime:
    return datetime.utcnow().replace(microsecond=0)

def _parse_time(time: str) -> Optional[datetime]:
    return datetime.strptime(time, TIME_FORMAT) if time != '' else None

def _format_time(time: Optional[datetime]) -> str:
    return datetime.strftime(time, TIME_FORMAT) if time is not None else ''


class JobStatus(Enum):
    NONEXISTENT = 'Nonexistent'  # never existed
    PENDING = 'Pending'  # in queue
    RUNNING = 'Running'  # taken from queue, being processed
    COMPLETED = 'Completed'  # processed succesfully
    FAILED = 'Failed'  # processed with error
    ARCHIVED = 'Archived' # protected against deletion
    DELETED = 'Deleted'  # processed, maximum storing period has elapsed and data have been removed


@dataclass
class JobStatusInfo(object):
    id: str = field(default_factory=_get_uuid)
    name: str = ''
    status: JobStatus = JobStatus.PENDING
    submission_time: datetime = field(default_factory=_get_current_time)
    start_time: Optional[datetime] = None
    end_time: Optional[datetime] = None
    delete_time: Optional[datetime] = None    

    @staticmethod
    def load(filename: Path) -> 'JobStatusInfo':
        d = {}
        with open(filename) as r:
            for line in r:
                line = line.strip()
                if line != '':
                    key, value = line.split('=', maxsplit=1)
                    d[key] = value
        submission_time=_parse_time(d['submission_time'])
        assert submission_time is not None
        result = JobStatusInfo(id=d['id'], 
                               name=d['name'], 
                               status=JobStatus(d['status']),
                               submission_time=submission_time,
                               start_time=_parse_time(d['start_time']),
                               end_time=_parse_time(d['submission_time']), 
                               delete_time=_parse_time(d['delete_time']))
        return result
    
    def save(self, filename: Path) -> None:
        with open(filename, 'w') as w:
            w.write(f'id={self.id}\n')
            w.write(f'name={self.name}\n')
            w.write(f'status={self.status.value}\n')
            w.write(f'submission_time={_format_time(self.submission_time)}\n')
            w.write(f'start_time={_format_time(self.start_time)}\n')
            w.write(f'end_time={_format_time(self.end_time)}\n')
            w.write(f'delete_time={_format_time(self.delete_time)}\n')

    @staticmethod
    def update(filename: Path, status: Optional[JobStatus] = None, submission_time: Optional[datetime] = None,
               start_time: Optional[datetime] = None, end_time: Optional[datetime] = None, delete_time: Optional[datetime] = None) -> 'JobStatusInfo':
        '''Update job status info in file filename and return the updated JobStatusInfo.'''
        info = JobStatusInfo.load(filename)
        if status is not None:
            info.status = status
        if submission_time is not None:
            info.submission_time = submission_time
        if start_time is not None:
            info.start_time = start_time
        if end_time is not None:
            info.end_time = end_time
        if delete_time is not None:
            info.delete_time = delete_time
        info.save(filename)
        return info
    
    @staticmethod
    def load_for_job(job_id: str) -> 'JobStatusInfo':
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_PENDING/job_id/JOB_STATUSINFO_FILE)
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_RUNNING/job_id/JOB_STATUSINFO_FILE)
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_COMPLETED/job_id/JOB_STATUSINFO_FILE)
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_FAILED/job_id/JOB_STATUSINFO_FILE)
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_ARCHIVED/job_id/JOB_STATUSINFO_FILE)
        with suppress(FileNotFoundError):
            return JobStatusInfo.load(DB_DIR_DELETED/job_id/JOB_STATUSINFO_FILE)
        return JobStatusInfo(id=job_id, status=JobStatus.NONEXISTENT)


