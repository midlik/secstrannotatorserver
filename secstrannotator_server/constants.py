import os
from pathlib import Path

from . import lib


try:
    SECSTRANNOTATOR_DLL = Path(os.environ['SECSTRANNOTATOR_DLL'])
    VAR_DIR = Path(os.environ['VAR_DIR'])
    QUEUE_NAME = os.environ['RQ_QUEUE']
    CIF_SOURCES = os.environ['CIF_SOURCES'].split()
except KeyError as ex:
    raise Exception(f'Environment variable {ex} must be defined before running this program.')

assert len(CIF_SOURCES) > 0, 'Environment variable CIF_SOURCES must contain at least one source'
for source in CIF_SOURCES:
    ok, error_msg = lib.validate_url_template(source)
    if not ok:
        raise Exception(error_msg)

try:
    # Directory with freely available static data (route /data maps to this dir, but in deployment it will be served by nginx)
    DATA_DIR = Path(os.environ['DATA_DIR'])
except KeyError as ex:
    DATA_DIR = VAR_DIR  / 'data'
TEMPLATES_DIR = DATA_DIR / 'templates'

JOB_TIMEOUT = int(os.environ.get('JOB_TIMEOUT', 86400))       # seconds, (86400 s = 24 hours)
JOB_CLEANUP_TIMEOUT = 600 # seconds, extra time for cleanup in case that job timed out
COMPLETED_JOB_STORING_DAYS = 14

LAST_UPDATE_FILE = DATA_DIR / 'db' / 'LAST_UPDATE.txt'
DB_DIR = VAR_DIR / 'jobs'
DB_DIR_PENDING = DB_DIR / 'Pending'
DB_DIR_RUNNING = DB_DIR / 'Running'
DB_DIR_COMPLETED = DB_DIR / 'Completed'
DB_DIR_FAILED = DB_DIR / 'Failed'
DB_DIR_ARCHIVED = DB_DIR / 'Archived'
DB_DIR_DELETED = DB_DIR / 'Deleted'

JOB_STATUSINFO_FILE = 'job_status.txt'
JOB_PARAMS = 'job_params.json'
JOB_DOMAINS_FILE = 'job_domain_list.txt'
JOB_DATADIR = 'data'
JOB_RESULTS_DIR = 'results'
JOB_DATAZIP = 'data.zip'
JOB_RESULT_FILE = 'result.txt'
JOB_STDOUT_FILE = 'stdout.txt'
JOB_STDERR_FILE = 'stderr.txt'
JOB_ERROR_MESSAGE_FILE = 'error_message.txt'

REFRESH_TIMES = [2, 4, 6, 8, 10, 15, 20, 30, 60, 120, 180, 300, 600, 1200, 1800, 3600]  # Times (in seconds) since submission when the waiting page should be autorefreshed, then each k*REFRESH_TIMES[-1] for any natural k 

TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

DEFAULT_FAMILY_EXAMPLE = '1.10.630.10'  # Cytochrome P450

MAX_CIF_SIZE_FOR_PYMOL = 15 * 1024**2  # 15 MiB

SECSTRANNOTATOR_OPTIONS = [
    '--sequence', 
    '--session', 
    '--unannotated',
    '--calculatevectors',
    '--soft',
    # '-k', '25,0.5,0.5'
]