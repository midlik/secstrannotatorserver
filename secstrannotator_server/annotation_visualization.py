from pathlib import Path
import json
from typing import List, Tuple, Dict, Optional, Any, Union, Literal

from .constants import DATA_DIR
from .annotations import REFERENCE_RESIDUE_GENERIC_NUMBER, is_helix, is_strand


VIS_HEADER = ''
# VIS_HEADER = '''
#  Resi Resn Sec.str.
#  ──── ──── ────────
# '''.lstrip('\n')


TOOLTIP_ATTRS = {'class': "btn-link", 'data-toggle': "tooltip", 'data-placement': "right", 'data-html': "true"}  #, 'data-offset': "0 130"}
POPOVER_ATTRS = {'class': "btn-link", 'data-toggle': "popover", 'data-placement': "right", 'data-html': "true", 
    'data-trigger': "manual", 'container': "body", 'tabindex': "0"}  #, 'data-trigger': "hover focus"


class HtmlTag(object):
    '''Represents a HTML tag, or a collection of HTML pieces (if tag is None)'''
    tag: Optional[str]  # tag type, or None if this is just a collection of HTML pieces
    void: bool  # this tag has no closing (like <br>)
    children: List[Union['HtmlTag', str]]  # HTML inside this tag
    attributes: Dict[str, str]
    def __init__(self, tag: Optional[str], *children: Union['HtmlTag', str], void: bool = False, **attributes: str) -> None:
        if tag is None:
            assert len(attributes) == 0
        if void is None:
            assert len(children) == 0
        self.tag = tag
        self.void = void
        self.children = list(children)
        self.attributes = attributes
    def __iadd__(self, child: Union['HtmlTag', str]) -> 'HtmlTag':
        if self.void:
            raise ValueError
        self.children.append(child)
        return self
    def str(self, plain_text: bool = False) -> str:
        '''Convert to HTML string (if not plain_string) or to plain text without HTML tags (if plain_string).'''
        children_str = ''.join(c.str(plain_text=plain_text) if isinstance(c, HtmlTag) else c for c in self.children)
        if plain_text or self.tag is None:
            return children_str
        else:
            tag = self.tag
            tag_parts = [tag]
            for attr, value in self.attributes.items():
                if '"' in value:
                    raise NotImplementedError
                tag_parts.append(f'{attr}="{value}"')
            tag_str = ' '.join(tag_parts)
            if self.void:
                return f'<{tag_str}>'
            else:
                return f'<{tag_str}>{children_str}</{tag}>'
    def __len__(self) -> int:
        '''Return length of the plain text.'''
        return len(self.str(plain_text=True))

Html = Union[str, HtmlTag]


def make_text_visualization(annotation_file: Path, sequence_file: Path, output_plain: Optional[Path] = None, output_html: Optional[Path] = None, include_sse_types: bool = False, template_name: Optional[str] = None) -> None:
    with open(annotation_file) as r:
        js = json.load(r)
    domain, annot = next(iter(js.items()))
    sses = annot['secondary_structure_elements']
    chain_ids = sorted(set(sse['chain_id'] for sse in sses))
    # assert len(chain_ids) == 1
    # the_chain = chain_ids.pop()
    col_chain = []
    col_resi: List[str] = []
    col_resn = []
    chain_shifts = {}
    for chain in chain_ids:
        seq, start_resi = read_sequence_json_file(sequence_file, chain)
        chain_shifts[chain] = start_resi - len(col_resi)
        for i, aa in enumerate(seq, start=start_resi):
            if aa is None:
                col_resi.append('')
                col_resn.append('-')
            else:
                col_resi.append(str(i))
                col_resn.append(aa)
        col_chain.append(chain)
        for i in range(len(seq)-1):
            col_chain.append('')
    n_lines = len(col_resi)
    col_clamp = [''] * n_lines
    col_label: List[Html] = [''] * n_lines
    col_gennum = [''] * n_lines
    col_logo: List[Html] = [''] * n_lines
    for sse in sses:
        chain = sse['chain_id']
        label = sse['label']
        start = sse['start']
        end = sse['end']
        middle = (start + end) // 2
        ref_resi = sse.get('reference_residue')
        for resi in range(start, end+1):
            i = resi - chain_shifts[chain]
            glyph = '──' if start==end else '─┐' if resi==start else '─┘' if resi==end else ' │'
            col_clamp[i] = glyph
            if resi==middle:
                col_label[i] = HtmlTag('strong', sse_label(sse, include_type=include_sse_types))
            if ref_resi is not None:
                generic_number = resi - ref_resi + REFERENCE_RESIDUE_GENERIC_NUMBER
                col_gennum[i] = f'{col_resn[i]}{resi}@{label}.{generic_number}'
            if resi==middle:
                logo_exists = template_name is not None and Path.is_file(DATA_DIR/'logos'/template_name/f'{label}.png')
                if logo_exists:
                    esc_label = label.replace("'", "&apos;").replace('"', '&quot;')
                    src = f'/data/logos/{template_name}/{esc_label}.png'
                    tooltip_header = f"Sequence logo from the whole family"
                    all_logos_href = f'/logos/{template_name}'
                    tooltip = f"<div><img class='tooltip-logo' src='{src}'><p style='text-align: center;'>(Reference residue numbered as 50.)<br><a target='_blank' href='{all_logos_href}'>Show all logos</a></p></div>"
                    col_logo[i] = HtmlTag('a', 'Sequence logo', **{'title': tooltip_header, 'data-content': tooltip.replace('&', '&amp;')}, **POPOVER_ATTRS)  # type: ignore

    chain_length = max_len(col_chain)
    resi_length = max_len(col_resi)
    label_length = max_len(col_label)
    gennum_length = max_len(col_gennum)
    logo_length = max_len(col_logo)
    total = HtmlTag(None, VIS_HEADER)
    for i in range(n_lines):
        if col_chain[i] != '' and i > 0:
            total += '─' * (chain_length + resi_length + 7)
            total += '\n'
        total += f'{col_chain[i].ljust(chain_length)} {col_resi[i].rjust(resi_length)} {col_resn[i]:^3}{col_clamp[i]:2} '
        total += col_label[i]
        total += ' ' * (label_length-len(col_label[i]))
        total += f'    {col_gennum[i].ljust(gennum_length)}   '
        total += col_logo[i]
        total += ' ' * (logo_length-len(col_logo[i]))
        total += '\n'  

    if output_plain is not None:
        with open(output_plain, 'w') as w:
            w.write(total.str(plain_text=True))
    if output_html is not None:
        with open(output_html, 'w') as w:
            w.write(total.str())


def split_sequence(sequence_string: str) -> List[Optional[str]]:
    '''Split amino acid sequence in one string into a list of amino acids,
    e.g. 'ML(MSE)P--K' -> ['M', 'L', 'MSE', 'P', None, None, 'K'] '''
    result: List[Optional[str]] = []
    building = False
    long_name = []
    for i, char in enumerate(sequence_string):
        if building:
            if char.isalnum():
                long_name.append(char)
            elif char==')':
                result.append(''.join(long_name))
                long_name.clear()
                building = False
            else:
                raise ValueError(f"Error parsing sequence on char {i}: '{char}'")
        else:
            if char.isalnum():
                result.append(char)
            elif char == '-':
                result.append(None)
            elif char=='(':
                building = True
            else:
                raise ValueError(f"Error parsing sequence on char {i}: '{char}'")
    if building:
        raise ValueError(f"Error parsing sequence - missing ')' at the end")
    return result

def read_sequence_json_file(file: Path, chain_id: str) -> Tuple[List[Optional[str]], int]:
    '''Return the list of amino acid names and the number of the first amino acid.'''
    with open(file) as r:
        js = json.load(r)
    for fragment in js:
        chain = fragment['chain_id']
        if chain == chain_id:
            start = fragment['start']
            end = fragment['end']
            sequence_str = fragment['sequence']
            sequence = split_sequence(sequence_str)
            assert len(sequence) == end - start + 1
            return (sequence, start)
    raise ValueError(f'Chain {chain_id} not found in the sequence file.')

def sse_label(sse: dict, include_type: bool = True) -> str:
    label = sse['label']
    parts = []
    if include_type:
        type_str = 'helix' if is_helix(sse) else 'strand' if is_strand(sse) else ''
        parts.append(type_str)
    if not label.startswith('_'):
        parts.append(label)
    return ' '.join(parts)

def max_len(elements) -> int:
    return max(len(elem) for elem in elements)
