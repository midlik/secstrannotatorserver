import sys
import argparse
from pathlib import Path
from typing import Dict, Union, Optional, Any


#  CONSTANTS  ################################################################################

CATEGORY = '_atom_site.'
DEFAULT_CHAIN = 'A'
DEFAULT_SYMBOL = 'C'
DEFAULT_ENTITY = 1

#  FUNCTIONS  ################################################################################

class AtomTable:
    def __init__(self):
        self.hetatm = []
        self.index = []
        self.name = []
        self.resn = []
        self.chain = []
        self.resi = []
        self.altloc = []
        self.x = []
        self.y = []
        self.z = []
        self.symbol = []
        self.entity = []

    def count(self):
        return len(self.index)

def read_pdb_line(line):
    if line[0:6]=='ATOM  ':
        hetatm = False
    elif line[0:6]=='HETATM':
        hetatm = True
    else:
        return None
    index = line[6:11]
    name  = line[12:16]
    altloc = line[16]
    resn  = line[17:20]
    chain = line[21]
    resi  = line[22:26]
    inscode = line[26]
    x = line[30:38]
    y = line[38:46]
    z = line[46:54]
    symbol = line[76:78]
    return (hetatm, int(index), name, altloc, resn, chain, int(resi), inscode, float(x), float(y), float(z), symbol)

def read_pdb(filename: Path) -> AtomTable:
    table = AtomTable()
    with open(filename) as f:
        for line in f:
            fields = read_pdb_line(line)
            if fields != None:
                hetatm, index, name, altloc, resn, chain, resi, inscode, x, y, z, symbol = fields
                if inscode != ' ':
                    print(f'Warning: ignoring atom with insertion code in PDB file {filename} ({index} {name} {resn} {resi}{inscode})', file=sys.stderr)
                    continue
                table.hetatm.append(hetatm)
                table.index.append(index)
                table.name.append(name)
                table.resn.append(resn)
                table.chain.append(chain)
                table.resi.append(resi)
                table.altloc.append(altloc)
                table.x.append(x)
                table.y.append(y)
                table.z.append(z)
                table.symbol.append(symbol)
    table.entity = [DEFAULT_ENTITY] * table.count()
    return table

def group_pdb_text(is_hetatm):
    return 'HETATM' if is_hetatm else 'ATOM'

def print_cif_minimal(atom_table, filename: Path, structure_name='structure'):
    table = atom_table
    group = ['HETATM' if het else 'ATOM' for het in table.hetatm]
    fields_values = [ 
        ('group_PDB', group), 
        ('id', table.index), 
        ('type_symbol', table.symbol), 
        ('label_atom_id', table.name), 
        ('label_alt_id', table.altloc), 
        ('label_comp_id', table.resn), 
        ('label_asym_id', table.chain), 
        ('label_entity_id', table.entity), 
        ('label_seq_id', table.resi), 
        ('auth_asym_id', table.chain), 
        ('Cartn_x', table.x), 
        ('Cartn_y', table.y), 
        ('Cartn_z', table.z) 
        ]
    with open(filename, 'w') as w:
        w.write('data_' + structure_name + '\n')
        w.write('loop_\n')
        for field, values in fields_values:
            w.write(CATEGORY + field + '\n')
        for i in range(table.count()):
            for field, values in fields_values:
                value = f'{values[i]:.3f}' if field.startswith('Cartn_') else str(values[i])
                w.write(value if value.strip() != '' else '.')
                w.write(' ')
            w.write('\n')

def convert_pdb_to_cif(input_pdb: Path, output_cif: Path, structure_name: str='Untitled') -> Optional[int]:
    '''Convert PDB-format file to mmCIF-format file'''
    atoms = read_pdb(input_pdb)
    print_cif_minimal(atoms, output_cif, structure_name=structure_name)
    return None

