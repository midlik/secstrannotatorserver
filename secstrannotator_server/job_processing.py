from pathlib import Path
import subprocess
import shutil
import requests
import json
import re
import gzip
from contextlib import suppress
from dataclasses import dataclass
from typing import List, Optional, Union, Literal

from .constants import JOB_DATADIR, JOB_RESULTS_DIR, JOB_DATAZIP, JOB_RESULT_FILE, JOB_STDOUT_FILE, JOB_STDERR_FILE, JOB_ERROR_MESSAGE_FILE, JOB_TIMEOUT, SECSTRANNOTATOR_DLL, TEMPLATES_DIR, CIF_SOURCES, SECSTRANNOTATOR_OPTIONS, DATA_DIR, MAX_CIF_SIZE_FOR_PYMOL
from . import lib
from . import pdb_to_cif
from . import annotations
from . import annotation_visualization

@dataclass
class JobParams(object):
    job_name: str
    template_type: Literal['standard', 'custom', 'none']
    template_name: str
    query_type: Literal['pdb', 'upload']
    query_name: str
    query_chain: Optional[str]
    query_range: str = ':'
    template_standard: Optional[str] = None
    query_pdbid: Optional[str] = None
    def to_json(self) -> str:
        return json.dumps(vars(self), indent=2)
    @classmethod
    def from_json(cls, json_string: str) -> 'JobParams':
        return cls(**json.loads(json_string))


def run_job(params: JobParams, job_dir: Path) -> bool:
    data_dir = job_dir/JOB_DATADIR
    try:
        if params.template_type == 'standard':
            try:
                shutil.copy(TEMPLATES_DIR/f'{params.template_name}.cif', data_dir/'template.cif')
                shutil.copy(TEMPLATES_DIR/f'{params.template_name}-template.sses.json', data_dir/'template-template.sses.json')
                with suppress(FileNotFoundError):
                    shutil.copy(TEMPLATES_DIR/f'{params.template_name}-alignment_matrices.json', data_dir/'template-alignment_matrices.json')
            except FileNotFoundError:
                raise Exception(f"Standard template '{params.template_name}' is not available.")
        elif params.template_type == 'custom':
            assert Path.is_file(data_dir/'template.cif')
            assert Path.is_file(data_dir/'template-template.sses.json')
            # template_name = try_extract_name(data_dir/'template.cif') or ''
        elif params.template_type == 'none':
            pass
        else:
            raise ValueError(f"'template_type' must be 'standard'|'custom', not '{params.template_type}'")

        if params.query_type == 'pdb':
            assert params.query_pdbid is not None
            get_structure(params.query_pdbid, data_dir/'query.cif', log_file=job_dir/JOB_RESULT_FILE)
        elif params.query_type == 'upload':
            if Path.is_file(data_dir/'query.cif'):
                # query_name = try_extract_name(data_dir/'query.cif') or ''
                pass
            elif Path.is_file(data_dir/'query.pdb'):
                pdb_to_cif.convert_pdb_to_cif(data_dir/'query.pdb', data_dir/'query.cif', structure_name = only_alnum(params.query_name) or 'Untitled')
            else:
                raise Exception('query structure file not found')
        else:
            raise ValueError(f"'query_type' must be 'pdb'|'upload', not '{params.query_type}'")
        # Path.write_text(data_dir/'summary.json', json.dumps({'template_name': params.template_name, 'query_name': params.query_name}))

        if params.template_type != 'none':
            template_chain = annotations.validate_template_annotation_file_and_get_chain(data_dir/'template-template.sses.json')
        skip_pymol_session = params.query_chain is None and Path.stat(data_dir/'query.cif').st_size > MAX_CIF_SIZE_FOR_PYMOL

        args: List[Union[str, Path]] = [
            lib.DOTNET, 
            SECSTRANNOTATOR_DLL, 
            data_dir, 
            f'template,{template_chain}' if params.template_type != 'none' else '--onlyssa',
            f'query,{params.query_chain},{params.query_range}' if params.query_chain != None else f'query',
            *SECSTRANNOTATOR_OPTIONS]
        if skip_pymol_session:
            with suppress(ValueError):
                args.remove('-s')
            with suppress(ValueError):
                args.remove('--session')
        proc = subprocess.run(args, timeout=JOB_TIMEOUT, capture_output=True, encoding='utf8')
        proc_returncode: Optional[int] = proc.returncode
        proc_stdout = proc.stdout
        proc_stderr = proc.stderr
        successfull = proc.returncode==0
        if successfull:
            if Path.is_file(data_dir/'template-alignment_matrices.json'):
                annotations.add_generic_numbering_to_annotation(data_dir/'query-annotated.sses.json', data_dir/'template-alignment_matrices.json', data_dir/'query-annotated_with_gen.sses.json')
                lib.move(data_dir/'query-annotated_with_gen.sses.json', data_dir/'query-annotated.sses.json')
            templ_sses: Optional[Path]
            if params.template_type != 'none':
                templ_sses = data_dir/'template-template.sses.json'
                query_sses = data_dir/'query-annotated.sses.json'
            else:
                templ_sses = None
                query_sses = data_dir/'query-detected.sses.json'
            annotations.make_summary(templ_sses, query_sses, data_dir/'summary.json')
            annotation_visualization.make_text_visualization(query_sses, data_dir/'query-sequence.json', 
                output_plain=data_dir/'query-vis.txt', output_html=data_dir/'query-vis-html.txt', include_sse_types=True, 
                template_name = params.template_name if params.template_type=='standard' else None)
    except subprocess.TimeoutExpired as ex:
        proc_returncode = None
        proc_stdout = anystr_to_str(ex.stdout)
        proc_stderr = anystr_to_str(ex.stderr)
        successfull = False
        Path.write_text(job_dir/JOB_ERROR_MESSAGE_FILE, f'Job timeout expired ({JOB_TIMEOUT} seconds).')
    except Exception as ex:
        proc_returncode = None
        proc_stdout = ''
        proc_stderr = str(ex)
        successfull = False
        if isinstance(ex, PDBEntryNotFoundError):
            error_message = f"Cannot find PDB entry '{ex.pdb_id}'. Please check that you provided correct PDB ID."
        elif isinstance(ex, annotations.InvalidAnnotationFileError):
            error_message = f"Provided template annotation file is not valid:\n\n{ex}"
        else:
            error_message = f"Unexpected error:\n{type(ex).__name__}: {ex}"
        Path.write_text(job_dir/JOB_ERROR_MESSAGE_FILE, error_message)

    with open(job_dir/JOB_RESULT_FILE, 'a') as w:
        if successfull:
            w.write('Completed\n\n')
        elif proc_returncode is not None: 
            w.write(f'Failed (exit code: {proc_returncode})\n\n')
        else: 
            w.write(f'Failed (timed out)\n\n')
        w.write(f'Stdout:\n{proc_stdout}\n\n')
        w.write(f'Stderr:\n{proc_stderr}\n\n')
    Path.write_text(job_dir/JOB_STDOUT_FILE, proc_stdout)
    Path.write_text(job_dir/JOB_STDERR_FILE, proc_stderr)

    if not successfull and proc_returncode is not None:
        err_msg = extract_error_message(proc_stderr)
        Path.write_text(job_dir/JOB_ERROR_MESSAGE_FILE, err_msg)

    shutil.copy(job_dir/JOB_STDOUT_FILE, data_dir/JOB_STDOUT_FILE)
    shutil.copy(job_dir/JOB_STDERR_FILE, data_dir/JOB_STDERR_FILE)

    if successfull:
        Path.mkdir(job_dir/JOB_RESULTS_DIR)
        lib.make_archive(data_dir, job_dir/JOB_RESULTS_DIR/JOB_DATAZIP)
        if params.template_type != 'none':
            RESULT_FILES = [
                'query-annotated.sses.json', 
                'query-vis.txt',
                'query-vis-html.txt',
                'summary.json',
                ]
            RESULT_FILES_OPTIONAL = [
                'query-annotated.pse', 
                'query-template.png', 
                'query-annotated.png',
            ]
        else:
            RESULT_FILES = [
                'query-detected.sses.json', 
                'query-vis.txt',
                'query-vis-html.txt',
                'summary.json',
                ]
            RESULT_FILES_OPTIONAL = [
                'query-detected.pse', 
                'query-detected.png',
                ]
        for file in RESULT_FILES:
            try:
                shutil.copy(data_dir/file, job_dir/JOB_RESULTS_DIR)
            except OSError:
                err_msg = f'Missing result file {file}'
                Path.write_text(job_dir/JOB_ERROR_MESSAGE_FILE, err_msg)
                return False
        for file in RESULT_FILES_OPTIONAL:
            try:
                shutil.copy(data_dir/file, job_dir/JOB_RESULTS_DIR)
            except OSError:
                pass
        shutil.rmtree(data_dir)
    
    return successfull


def anystr_to_str(string: Union[str, bytes]) -> str:
    if isinstance(string, str):
        return string
    elif isinstance(string, bytes):
        try:
            return string.decode('utf8')
        except UnicodeDecodeError:
            return str(string)
    else:
        return str(string)

def extract_error_message(stderr: str) -> str:
    error_lines = [line.strip() for line in stderr.splitlines() if line.strip() != '']
    if len(error_lines) > 0:
        last_line = error_lines[-1]
        if ':' in last_line:
            return last_line.split(':', maxsplit=1)[1].strip()
        else:
            return last_line
    else:
        return ''

def get_structure(pdb: str, outfile: Path, log_file: Optional[Path] = None) -> List[str]:
    tried_urls: List[str] = []
    for source in CIF_SOURCES:
        url = lib.format_url_for_pdb(source, pdb)
        content = try_get_url(url)
        if content is not None:
            Path(outfile).write_bytes(content)
            with open(log_file, 'a') as w: 
                w.write(f'[INFO]  Tried structure URL: {url} OK\n')
            return
        else:
            tried_urls.append(url)
            with open(log_file, 'a') as w: 
                w.write(f'[INFO]  Tried structure URL: {url} Failed\n')
    print(f'Could not find {pdb} here:', *tried_urls, sep='\n    ')
    with open(log_file, 'a') as w:
        w.write(f'[ERROR] Failed to get PDB structure {pdb}\n')
    raise PDBEntryNotFoundError(f"Cannot find PDB entry '{pdb}'", pdb_id=pdb)

def try_get_url(url: str) -> Optional[bytes]:
    if url.startswith('http://'):
        response = requests.get(url)
        if response.ok:
            content = response.content
        else:
            return None
    elif url.startswith('file://'):
        path = url[len('file://'):]
        try:
            content = Path(path).read_bytes()
        except OSError:
            return None
    else:
        raise ValueError(f'Invalid source "{url}". Sources in CIF_SOURCES must start with http:// or file://. Sources ending with .gz will be decompressed by gzip.')
    if url.endswith('.gz'):
        return gzip.decompress(content)
    else:
        return content

class PDBEntryNotFoundError(Exception):
    pdb_id: str
    def __init__(self, *args: object, pdb_id: str) -> None:
        super().__init__(*args)
        self.pdb_id = pdb_id

def try_extract_name(cif_file: Path) -> Optional[str]:
    re_data = re.compile(r'\s*data_(\w+)')
    re_entry_id = re.compile(r'\s*_entry\.id +(\w+)')
    with open(cif_file) as r:
        for line in r:
            match = re_data.match(line) or re_entry_id.match(line)
            if match is not None:
                return match.group(1)
    return None

def only_alnum(string: str) -> str:
    return ''.join(c for c in string if c.isalnum())
