from pathlib import Path
import shutil
import sys
import subprocess
from typing import List, Tuple


def move(src: Path, dest: Path) -> Path:
    '''Move file'''
    return Path(shutil.move(str(src), str(dest)))

def make_archive(src: Path, dest: Path) -> Path:
    '''Make archive, e.g. make_archive(Path('stuff/'), Path('stuff.zip'))'''
    fmt = dest.suffix.lstrip('.')
    archive = shutil.make_archive(str(dest.with_suffix('')), fmt, str(src))
    return Path(archive)

def trim_with_ellipsis(string: str, max_length: int = 200, ellipsis: str = '...') -> str:
    if 0 <= max_length < len(string):
        return string[:max_length] + ellipsis
    else:
        return string

def format_url_for_pdb(url_template: str, pdb: str) -> str:
    assert len(pdb) >= 4, f'PDB ID must have at least 4 characters, "{pdb}" is invalid PDB ID'
    mark_dict = {'pdb': pdb, 'pdb_0': pdb[0], 'pdb_1': pdb[1], 'pdb_2': pdb[2], 'pdb_3': pdb[3]}
    try:
        return url_template.format(**mark_dict)
    except KeyError as ex:
        key = ex.args[0]
        valid_marks_str = ', '.join(f'{{{mark}}}' for mark in mark_dict.keys())
        raise ValueError(f'Invalid source in CIF_SOURCES: "{url_template}" contains invalid mark {{{key}}} (valid marks: {valid_marks_str})')

def validate_url_template(url_template: str) -> Tuple[bool, str]:
    try:
        format_url_for_pdb(url_template, '0123')
        return (True, '')
    except ValueError as ex:
        return (False, str(ex))

def _find_dotnet(candidates: List[str|Path], version: str) -> str|Path:
    for candidate in candidates:
        try:
            result = subprocess.run([candidate, '--list-runtimes'], check=True, capture_output=True, encoding='utf8')# stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            assert version in result.stdout, f'Dotnet candidate "{candidate}" does not have the correct runtime version ({version})'
            return candidate
        except (subprocess.CalledProcessError, FileNotFoundError, AssertionError):
            pass
    raise FileNotFoundError(f'Cannot find dotnet, version {version} (tried these: {candidates})')


DOTNET_VERSION = 'Microsoft.NETCore.App 6.0'
DOTNET = _find_dotnet(['dotnet', str(Path.home()/'.dotnet'/'dotnet')], DOTNET_VERSION)
