import json
import jsonschema  # type: ignore
from pathlib import Path
from typing import List, Tuple, Dict, Optional, Any, Union

from . import lib


REFERENCE_RESIDUE_GENERIC_NUMBER = 50

ANNOTATION_SCHEMA = {
    'type': 'object',
    'minProperties': 1,
    'maxProperties': 1,
    'additionalProperties': {
        'type': 'object',  # Domain annotation
        'required': ['secondary_structure_elements'],
        'properties': {
            'secondary_structure_elements': {
                'type': 'array',  # List of SSEs
                'minItems': 1,
                'items': {
                    'type': 'object',  # SSE
                    'required': ['label', 'chain_id', 'start', 'end', 'type']
                }
            },
            'beta_connectivity': {
                'type': 'array',  # Beta-connectivity
                'items': {
                    'type': 'array',  # Beta-ladder
                    'prefixItems': [
                        {'type': ['string', 'number']},  # First strand of the ladder
                        {'type': ['string', 'number']},  # Second strand of the ladder
                        {'type': 'number', 'enum': [1, -1]},  # Ladder orientation
                    ]
                }
            }
        }
    }
}


class InvalidAnnotationFileError(Exception):
    def __init__(self, header: Optional[str] = None, text: Optional[str] = None, main_message: Optional[str] = None, *args: object) -> None:
        lines = []
        if header is not None:
            lines.append(header)
        if text is not None:
            lines.append('='*80)
            lines.append(lib.trim_with_ellipsis(text))
            lines.append('='*80)
        if main_message is not None:
            lines.append(main_message)
        total_message = '\n'.join(lines)
        super().__init__(total_message, *args)


def validate_template_annotation_file_and_get_chain(sses_json_file: Path) -> str:
    '''Raise InvalidAnnotationFileError if the file is not valid, 
    return the chain ID of the only annotated chain if the file is valid.'''
    try:
        text = sses_json_file.read_text()
    except UnicodeDecodeError:
        raise InvalidAnnotationFileError('Not a valid text file with UTF-8 encoding (might be a binary file)')
    try:
        js = json.loads(text)
    except json.JSONDecodeError:
        raise InvalidAnnotationFileError('Invalid JSON file', text)
    try:
        jsonschema.validate(js, ANNOTATION_SCHEMA)
    except jsonschema.ValidationError as ex:
        message = str(ex)
        str_instance = str(ex.instance)
        if message.startswith(str_instance):
            message = message.replace(str_instance, str_instance+'\n')
        raise InvalidAnnotationFileError('Invalid annotation file', text, message)

    # key, annot = next(iter(js.items()))
    # sses = annot['secondary_structure_elements']
    sses = read_annotation_sses(js)
    if len(sses) == 0:
        raise InvalidAnnotationFileError('Invalid annotation file', text, 'Must contain at least one SSEs in "secondary_structure_elements".')
    chain_ids = sorted(set(sse['chain_id'] for sse in sses))
    if len(chain_ids) > 1:
        chain_ids_str = ', '.join(chain_ids)
        raise InvalidAnnotationFileError('Invalid annotation file', text, f'All annotated SSEs must be from the same chain (encountered chains: {chain_ids_str}).')
    the_chain = chain_ids[0]
    return the_chain

def add_generic_numbering_to_annotation(annotation_file: Path, alignment_matrices: Path, output_file: Path) -> None:
    with open(annotation_file) as r:
        js = json.load(r)
    with open(alignment_matrices) as r:
        alignments = json.load(r)
    for domain, annot in js.items():
        for sse in annot['secondary_structure_elements']:
            label = sse['label']
            sequence = sse.get('sequence')
            if sequence is not None and label in alignments:
                alphabet = alignments[label]['alphabet']
                scores = alignments[label]['scores']
                logo_first_index = alignments[label]['first_index']
                shift = align_by_scores(sequence, alphabet, scores)
                resi_ref = REFERENCE_RESIDUE_GENERIC_NUMBER - logo_first_index + sse['start'] - shift
                sse['reference_residue'] = resi_ref
    with open(output_file, 'w') as w:
        json.dump(js, w, indent=1)

def align_by_scores(sequence: str, alphabet: List[str], scores: List[List[float]]) -> int:
    '''Return optimal shift to align sequence with the scores matrix representing 
    a sequence alignment (scores = probabilities @ substitution_matrix).
    Positive shift means the sequence should be shifted to the right.'''
    UNKNOWN_LETTER = '?'
    letter_index = {letter: i for i, letter in enumerate(alphabet)}
    t = len(scores) 
    m = len(sequence)
    min_shift = -m+1
    max_shift = t-1
    candidates = []
    for shift in range(min_shift, max_shift+1):
        fro = max(0, shift)
        to = min(t, m+shift)
        score = 0.0
        for i in range(fro, to):
            letter = sequence[i-shift]
            if letter in letter_index:
                score += scores[i][letter_index[letter]]
            elif UNKNOWN_LETTER in letter_index:
                score += scores[i][letter_index[UNKNOWN_LETTER]]
        candidates.append((score, shift))
    best_score, best_shift = max(candidates)
    return best_shift


def read_annotation_sses(input_json: Union[Path, dict]) -> List[dict]:
    if isinstance(input_json, dict):
        js = input_json
    else:
        with open(input_json) as r:
            js = json.load(r)
    assert len(js) == 1, 'Annotation JSON must contain exactly one key-value pair.'
    annot = next(iter(js.values()))
    sses = annot['secondary_structure_elements']
    return sses

def is_helix(sse: Dict) -> bool:
    return sse['type'].upper() in 'GHI'
    
def is_strand(sse: Dict) -> bool:
    return sse['type'].upper() in 'EB'
    
def count_sses_helices_strands(sses: List[dict]) -> Tuple[int, int, int]:
    n_all = n_helices = n_strands = 0
    for sse in sses:
        n_all += 1
        if is_helix(sse):
            n_helices += 1
        elif is_strand(sse):
            n_strands += 1
    return n_all, n_helices, n_strands

def make_summary(template_annotation: Optional[Path], query_annotation: Path, output: Path) -> None:
    if template_annotation is not None:
        template_sses = read_annotation_sses(template_annotation)
        query_sses = read_annotation_sses(query_annotation)
        query_annotated_sses = [sse for sse in query_sses if not sse['label'].startswith('_')]
    else:
        template_sses = []
        query_sses = read_annotation_sses(query_annotation)
        query_annotated_sses = []
    result = {}
    result['n_template_sses'], result['n_template_helices'], result['n_template_strands'] = count_sses_helices_strands(template_sses)
    result['n_detected_sses'], result['n_detected_helices'], result['n_detected_strands'] = count_sses_helices_strands(query_sses)
    result['n_annotated_sses'], result['n_annotated_helices'], result['n_annotated_strands'] = count_sses_helices_strands(query_annotated_sses)
    with open(output, 'w') as w:
        json.dump(result, w, indent=4)