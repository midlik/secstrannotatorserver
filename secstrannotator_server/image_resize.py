from PIL import Image  # type: ignore
from pathlib import Path
import argparse
from typing import Union, Optional, Dict, Any

def resize_image(input: Path, output: Path, width: Optional[int] = None, height: Optional[int] = None):
    if width is None and height is None:
        raise ValueError(f'Must specify width, height, or both.')
    im = Image.open(input)
    w, h = im.size
    if width is None:
        width = int(height * w / h)
    elif height is None:
        height = int(width * h / w)
    im = im.resize((width, height), Image.ANTIALIAS)
    im.save(output)

def resize_images(input_directory: Union[Path, str], extension: str, output_directory: Union[Path, str], width: Optional[int] = None, height: Optional[int] = None) -> None:
    if not extension.startswith('.'):
        extension = '.' + extension
    Path(output_directory).mkdir(parents=True, exist_ok=True)
    files = sorted(Path(input_directory).glob(f'*{extension}'))
    for file in files:
        outfile = Path(output_directory, file.name)
        resize_image(file, outfile, width=width, height=height)
    print(f'Resized {len(files)} files')

def parse_args() -> Dict[str, Any]:
    '''Parse command line arguments.'''
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_directory', help='Directory with input files', type=str)
    parser.add_argument('extension', help='Extension of the input files', type=str)
    parser.add_argument('output_directory', help='Directory for output files', type=str)
    parser.add_argument('--width', help=f'Width of the output images (must specify width, height, or both)', type=int, default=None)
    parser.add_argument('--height', help=f'Height of the output images (must specify width, height, or both)', type=int, default=None)
    args = parser.parse_args()
    return vars(args)

def main(input_directory: Union[Path, str], extension: str, output_directory: Union[Path, str], width: Optional[int] = None, height: Optional[int] = None) -> Optional[int]:
    resize_images(input_directory, extension, output_directory, width=width, height=height)
    return None

if __name__ == '__main__':
    args = parse_args()
    exit_code = main(**args)
    if exit_code is not None:
        exit(exit_code)


# FINAL_HEIGHT = 300

# im = Image.open('/server_data/annotator_data/data/logos/2nnjA/A.png')
# w, h = im.size
# final_width = int(FINAL_HEIGHT * w / h)
# im = im.resize((final_width, FINAL_HEIGHT), Image.ANTIALIAS)
# im.save('/server_data/annotator_data/data/logos/2nnjA/A2.png')