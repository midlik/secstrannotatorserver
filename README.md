# SecStrAnnotator Server

**SecStrAnnotator Server** allows using **SecStrAnnotator** as a web application without need to install locally.

## Running SecStrAnnotator Server

1. Install Docker on your server machine:

    ```bash
    sudo apt-get update -y
    sudo apt-get install docker docker.io docker-compose
    sudo usermod -aG docker $USER
    ```

    Log out and log in again

2. Pull the Docker image:

    ```bash
    docker pull registry.gitlab.com/midlik/secstrannotatorserver
    ```

3. Copy `docker-compose.yaml` to your server machine and change the settings appropriately (set ports, volume mount-points...).

4. Start the container:

    ```bash
    docker-compose -f PATH_TO_DOCKER_COMPOSE_YAML up -d
    docker logs secstrannotatorserver_secstrannotatorserver_1
    # docker-compose -f PATH_TO_DOCKER_COMPOSE_YAML down  # stop the container
    ```
