#!/bin/bash
set -e

echo 'startup-docker.sh'

SW_DIR=$(realpath $(dirname $0))
cd $SW_DIR

ROOT_DIR='/srv'
INIT_DIR="$SW_DIR/init"
export VAR_DIR='/srv/var'
export DATA_DIR='/srv/data'
export SSL_CERT='/srv/ssl/certificate.pem'
export SSL_KEY='/srv/ssl/key.pem'
export SECSTRANNOTATOR_DLL='/srv/bin/SecStrAnnot2/bin/Release/net6.0/SecStrAnnotator.dll'
export RQ_QUEUE='sestra_jobs'
export GUNICORN_PORT='4000'

test -z "$N_RQ_WORKERS" && echo "Error: environment variable N_RQ_WORKERS is not set" && exit 1
test -z "$N_GUNICORN_WORKERS" && echo "Error: environment variable N_GUNICORN_WORKERS is not set" && exit 1
test -z "$CIF_SOURCES" && echo "Error: environment variable CIF_SOURCES is not set" && exit 1

test -z "$HTTP_PORT" && test -z "$HTTPS_PORT" && echo "Error: at least one of HTTP_PORT, HTTPS_PORT must be set" && exit 1
test -n "$HTTPS_PORT" -a ! -f "$SSL_CERT" && echo "Error: when HTTPS_PORT is set, SSL certificate must be mounted to $SSL_CERT" && exit 1
test -n "$HTTPS_PORT" -a ! -f "$SSL_KEY" && echo "Error: when HTTPS_PORT is set, SSL private key must be mounted to $SSL_KEY" && exit 1

# Preparation
source venv/bin/activate
RQ=$(which rq)  ||  { echo "rq is not correctly installed" && exit 1; }
GUNICORN=$(which gunicorn)  ||  { echo "gunicorn is not correctly installed" && exit 1; }
NGINX=$(which nginx)  ||  { echo "nginx is not correctly installed" && exit 1; }

echo "VAR_DIR=$VAR_DIR"
echo "DATA_DIR=$DATA_DIR"

cp -r $INIT_DIR/* $ROOT_DIR

START_TIME=$(date -u +%Y%m%d_%H%M%S)
LOG_DIR="$VAR_DIR/logs/run_$START_TIME"
echo "Logs: $LOG_DIR"
PROC_DIR="$VAR_DIR/running_processes"
rm -rf $PROC_DIR
mkdir -p $PROC_DIR

# Start Gunicorn
echo "Starting Gunicorn"
mkdir -p "$LOG_DIR/gunicorn"
gunicorn -w $N_GUNICORN_WORKERS -b 127.0.0.1:$GUNICORN_PORT secstrannotator_server:app > "$LOG_DIR/gunicorn/out.txt" 2> "$LOG_DIR/gunicorn/err.txt" & 
GUNICORN_PROC=$!
touch $PROC_DIR/$GUNICORN_PROC
sleep 2
ps -p $GUNICORN_PROC > /dev/null || { echo 'Failed to start gunicorn'; cat "$LOG_DIR/gunicorn/err.txt"; exit 1; }

# Start RedisQueue workers
echo "Starting RedisQueue"
mkdir -p "$LOG_DIR/rq"
service redis-server start
for RQ_WORKER in $(seq -w 1 $N_RQ_WORKERS)
do
    $RQ worker $RQ_QUEUE > "$LOG_DIR/rq/worker_$RQ_WORKER.out.txt" 2> "$LOG_DIR/rq/worker_$RQ_WORKER.err.txt" & 
    RQ_WORKER_PROC=$!
    touch $PROC_DIR/$RQ_WORKER_PROC
    ps -p $RQ_WORKER_PROC > /dev/null || { echo "Failed to start RQ worker $RQ_WORKER"; cat "$LOG_DIR/rq/worker_$RQ_WORKER.err.txt"; exit 1; }
done

# Start Nginx
echo "Startinx Nginx"
export NGINX_LOG_DIR="$LOG_DIR/nginx"
mkdir -p $NGINX_LOG_DIR
NGINX_CONF_DIR="$VAR_DIR/nginx_conf"
mkdir -p $NGINX_CONF_DIR
cp -r $SW_DIR/nginx/* $NGINX_CONF_DIR/
test -n "$HTTP_PORT" && export MAYBE_INCLUDE_HTTP="include $NGINX_CONF_DIR/nginx-http.conf;"
test -n "$HTTP_PORT" -a "$HTTP_REDIRECT_TO_HTTPS" -gt "0" && export MAYBE_INCLUDE_HTTP="include $NGINX_CONF_DIR/nginx-http-redirect.conf;"
test -n "$HTTPS_PORT" && export MAYBE_INCLUDE_HTTPS="include $NGINX_CONF_DIR/nginx-https.conf;"
ENVS='$NGINX_LOG_DIR $SSL_CERT $SSL_KEY $HTTP_PORT $HTTPS_PORT $GUNICORN_PORT $MAYBE_INCLUDE_HTTP $MAYBE_INCLUDE_HTTPS'
for F in $NGINX_CONF_DIR/*.template.conf; do envsubst "$ENVS" < $F > ${F%.template.conf}.conf; done
NGINX_CONF=$NGINX_CONF_DIR/nginx.conf
nginx -c $NGINX_CONF

# Print some logs:
echo "N_GUNICORN_WORKERS=$N_GUNICORN_WORKERS"
echo "N_RQ_WORKERS=$N_RQ_WORKERS"
echo "HTTP_PORT=$HTTP_PORT"
echo "HTTPS_PORT=$HTTPS_PORT"
echo "CIF_SOURCES=$CIF_SOURCES"

sleep 2
echo 'Started processes:'
ps -A | grep 'gunicorn' || echo 'Warning: no "gunicorn" running'
ps -A | grep 'redis' || echo 'Warning: no "redis" running'
ps -A | grep 'rq' || echo 'Warning: no "rq" running'
ps -A | grep 'nginx' || echo 'Warning: no "nginx" running'

sleep infinity
