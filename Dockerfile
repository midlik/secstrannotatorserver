FROM ubuntu:22.04

RUN apt-get update -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y tzdata
RUN apt-get install -y git

# # Debug:
# RUN apt-get install -y wget iputils-ping iputils-tracepath traceroute less

RUN mkdir -p /srv
RUN mkdir -p /srv/bin
RUN mkdir -p /srv/var
RUN mkdir -p /srv/var/jobs
RUN mkdir -p /srv/var/logs
RUN mkdir -p /srv/data

WORKDIR /srv/bin

COPY ./install.sh secstrannotatorserver/install.sh
COPY ./dotnet-install.sh secstrannotatorserver/dotnet-install.sh
COPY ./requirements.txt secstrannotatorserver/requirements.txt
RUN bash secstrannotatorserver/install.sh --clean --no-sudo
COPY . secstrannotatorserver

# COPY . secstrannotatorserver
# RUN bash secstrannotatorserver/install.sh

RUN git clone https://gitlab.com/midlik/SecStrAnnot2.git

ENV N_RQ_WORKERS="8"
ENV N_GUNICORN_WORKERS="4"
ENV HTTP_PORT="80"
ENV HTTPS_PORT=""
ENV HTTP_REDIRECT_TO_HTTPS="0"
ENV CIF_SOURCES=""
ENV JOB_TIMEOUT="86400"
ENV CIF_SOURCES="file:///srv/pdb/mmCIF/{pdb_1}{pdb_2}/{pdb}.cif.gz  http://www.ebi.ac.uk/pdbe/entry-files/download/{pdb}_updated.cif.gz  http://www.ebi.ac.uk/pdbe/entry-files/download/{pdb}_updated.cif"

ENTRYPOINT ["bash", "/srv/bin/secstrannotatorserver/startup-docker.sh"]
