# Overview of file system for an SecStrAnnotatorServer docker container

```md

/srv/
    bin/
        SecStrAnnot2/  **SecStrAnnotator2 git repository**
        secstrannotatorserver/  **SecStrAnnotatorServer git repository**
    var/  **$VAR_DIR - to be mounted** 
        nginx.conf
        logs/
            run_$START_TIME/  **$LOG_DIR**
                gunicorn/
                    out.txt
                    err.txt
                rq/
                    worker_*.out.txt
                    worker_*.err.txt
                nginx/
                    access.log
                    error.log
        running_processes/
            $PID
        jobs/
            Pending/
            Running/
            Completed/
            Failed/
            Archived/
            Deleted/
    data/ **Static data served by Nginx - to be mounted**
        logos/
            2nnjA/
                *.png
            2nnjA-fullsize/
                *.png
        templates/
            2nnjA.cif
            2nnjA-template.sses.json
            2nnjA-alignment_matrices.json
    ssl/  **SSL certificate - to be mounted**
        certificate.pem
        key.pem
    pdb/ **PDB mirror (can be elsewhere) - to be mounted**
        mmCIF/
            tq/, ...
                1tqn.cif.gz, ...
            
```
