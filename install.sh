#!/bin/bash
set -e
DIR=$(dirname $0)
cd $DIR

CLEAN='false'
SUDO='sudo'

for ARG in $@; do
    if [ "$ARG" = "--clean" ]; then CLEAN='true'; fi;
    if [ "$ARG" = "--no-sudo" ]; then SUDO=''; fi;
done;

# Install apt packages
$SUDO apt-get update -y
$SUDO apt-get install -y --no-install-recommends python3-venv nginx redis-server pymol gettext-base

# Install .NET (for SecStrAnnotator)
if dotnet --info; then
    echo "secstrannotatorserver/install.sh: Dotnet already installed"
else
    echo "secstrannotatorserver/install.sh: Trying to install Dotnet..."
    $SUDO apt-get install -y curl
    $SUDO apt-get install -y libicu-dev
    ./dotnet-install.sh -c 6.0
    PATH="$PATH:$HOME/.dotnet"
    echo -e '\n# Add Dotnet to PATH:\nPATH="$PATH:$HOME/.dotnet"' >> $HOME/.bashrc
fi

if dotnet --info; then
    echo "secstrannotatorserver/install.sh: Dotnet successfully installed"
else
    echo "secstrannotatorserver/install.sh: Dotnet installation failed"
    exit 1
fi

# Install Python virtual environment
if [ "$CLEAN" = "true" ]; then rm -rf venv/; fi;
python3 -m venv venv
. venv/bin/activate
python3 -m pip install -r requirements.txt
python3 -m pip freeze > requirements-actual.txt

# Hack (PyMOL cannot be installed through pip):
DIR=$(echo $VIRTUAL_ENV/lib/python3.*/site-packages)
ln -s "/usr/lib/python3/dist-packages/pymol" "$DIR/pymol"
ln -s "/usr/lib/python3/dist-packages/chempy" "$DIR/chempy"
ln -s "/usr/lib/python3/dist-packages/pymol2" "$DIR/pymol2"
